"use strict";

var config = require('config');

var async = require('async');
var url = require("url");
var path = require("path");
var crypto = require('crypto');
var glob = require('glob');

var express = require('express');
var router = express.Router();

const db = require('../../models/db');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const uuidv4 = require('uuid/v4');

router.get("/spaces", async function(req, res, next){
    try{
       let data =  await db.Space.findAll({
           where:{
               edit_hash:{ [Op.not]: null}
           },
           attributes: ["_id", "edit_hash"]
       });
        return res.send({
            "statusCode":"200", 
            "message": "success",
            "data":data
        })
    }catch(error){
        throw error
    }
})

router.post("/create_new_canvas", async (req, res, next)=>{
    try{
        let attr = req.body;
        attr._id = uuidv4();
        attr.password = crypto.randomBytes(64).toString('hex').substring(0, 7);
        console.log(attr);
        db.Canvases.create(attr).then(create_s =>{
            console.log(create_s)
            return res.send({
                "statusCode": "200",
                "message": "create successfully!",
                "data": create_s
            })
        })
    }catch(error){
        throw error
    }
})

router.put("/canvases/:canvas_id/update", async (req, res, next)=>{
    try{
        let param_ = req.params
        let attr = req.body;
        
        db.Canvases.update({ canvas_doc: attr.canvas_doc }, {
            where: {
              _id: param_.canvas_id
            }
          }).then(create_s => {
              return res.send({
                "statusCode": "200",
                "message": "create successfully!",
                "data": create_s
              })
          })
        return 
    }catch(error){
        throw error
    } 
})

module.exports = router;